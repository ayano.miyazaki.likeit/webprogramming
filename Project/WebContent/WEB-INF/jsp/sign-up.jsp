<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザ新規登録</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

</head>

<body>

	<div class="header">
		<ul>
			<li class="log-out">ログアウト</li>
			<li class="header-user">${userInfo.name }さん</li>
		</ul>
	</div>

	<p class="title">ユーザ新規登録</p>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<form class="form" action="SignUpServlet" method="post">
		<div class="form-group">
			<label class="label">ログインID</label>
				<input class="text-input" type="text" name="loginId"
					value=${loginId }>
		</div>

		<div class="form-group">
			<label class="label">パスワード</label> <input class="text-input"
				type="password" name="password">
		</div>

		<div class="form-group">
			<label class="label">パスワード(確認)</label> <input class="text-input"
				type="password" name="passwordCheck" value="">
		</div>

		<div class="form-group">
			<label class="label">ユーザ名</label>
				<input class="text-input" type="text" name="name" value=${name }>

		</div>

		<div class="form-group">
			<label class="label">生年月日</label>
				<input class="text-input" type="date" name="birthDate"
					value=${birthDate }>
		</div>


		<button type="submit">登録</button>


	</form>

	<button type="button" class="return" onclick="history.back()">戻る</button>

</body>
</html>