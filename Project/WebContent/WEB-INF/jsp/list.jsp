<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>


<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

</head>

<body>

	<div class="header">
		<ul>
			<li class="log-out"><a href="LogoutServlet">ログアウト</a></li>
			<li class="header-user">${userInfo.name}さん</li>
		</ul>
	</div>

	<p class="title">ユーザ一覧</p>
	<p class="to-sign-up">
			<a href="SignUpServlet?id=${user.id }">新規登録</a>
	</p>

	<form action="UserListServlet" method="post">
		<div class="form-group">
			<label class="label">ログインID</label> <input class="text-input"
				name="login-id" type="text">
		</div>

		<div class="form-group">
			<label class="label">ユーザ名</label> <input class="text-input"
				name="user-name" type="text">
		</div>

		<div class="form-group">
			<label class="label">生年月日</label> <input class="date-input"
				type="date" name="birth-date-start"> <input
				class="date-input" type="date" name="birth-date-end">
		</div>

		<div class="button">
			<button type="submit">検索</button>
		</div>

	</form>
	<br>
	<hr>
	<br>

	<table align="center" class="table table-striped">
		<thead>
			<tr>
				<th scope="col">ログインID</th>
				<th scope="col">ユーザ名</th>
				<th scope="col">生年月日</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<!-- TODO 未実装；ログインボタンの表示制御を行う -->
					<td><a class="btn btn-primary"
						href="UserDetailServlet?id=${user.id}">詳細</a> <a
						class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
						<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>