<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

</head>

<body>

	<div class="header">
		<ul>
			<li class="log-out"><a href="WEB-INF/jsp/login.jsp">ログアウト</a></li>
			<li class="header-user">${user.name } さん</li>
		</ul>
	</div>

	<div class="title">ユーザ情報更新</div>

	<form class="form" action="UpdateServlet" method="post">
		<div class="form-group">
			<label class="label">ログインID</label>
			<div class="info" name="loginId">${user.loginId }</div>
		</div>


		<div class="form-group">
			<label class="label">パスワード</label> <input class="text-input"
				type="password" name="password">
		</div>

		<div class="form-group">
			<label class="label">パスワード(確認)</label> <input class="text-input"
				type="password" name="passwordCheck">
		</div>

		<div class="form-group">
			<label class="label">ユーザ名</label> <input class="text-input"
				type="text" name="name" value=${user.name }>
		</div>

		<div class="form-group">
			<label class="label">生年月日</label> <input class="text-input"
				type="date" name="birthDate" value=${user.birthDate }>
		</div>


		<button type="submit">更新</button>


	</form>

	<p class="return"><a href=>戻る</a></p>

</body>
</html>