<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
</head>

<body>

	<div class="header">
		<ul>
			<li class="log-out">ログアウト</li>
			<li class="header-user">${user.name } さん</li>
		</ul>
	</div>

	<div class="title">ユーザ情報詳細参照</div>

	<form class="form" action = "UserDetailServlet" method = "post">

		<div class="form-group">
			<label class="label">ログインID</label>
			<div class="info" >${user.loginId}</div>
		</div>

		<div class="form-group">
			<label class="label">ユーザ名</label>
			<div class="info" >${user.name}</div>
		</div>

		<div class="form-group">
			<label class="label">生年月日</label>
			<div class="info">${user.birthDate}</div>
		</div>

		<div class="form-group">
			<label class="label">登録日時</label>
			<div class="info" >${user.createDate}</div>
		</div>

		<div class="form-group">
			<label class="label">更新日時</label>
			<div class="info">${user.updateDate}</div>
		</div>

		<button type = "button" class="return"  onclick = "history.back()">戻る</button>

	</form>



</body>
</html>