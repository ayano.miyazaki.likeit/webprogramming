<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>

    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/boaotstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
		<link rel="stylesheet"
		  href="style.css">

</head>

<body>

<p class="title">ログイン画面</p>

<form class="form" action = "LoginServlet" method = "post">
  <div class="form-group">
    <label class="label">ログインID</label>
    <input class="text-input" type="text" name="loginId">
  </div>

  <div class="form-group">
    <label class="label">パスワード</label>
    <input class="text-input" type="password" name="password">
  </div>

<div class="submit">
  <button type="submit"  value="login">ログイン</button>
</div>

</form>

</body>
</html>