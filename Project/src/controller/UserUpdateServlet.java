package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		User user = userDao.findById(id);

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
//		// 確認用：idをコンソールに出力
//		System.out.println(id);
//
//
//		//各項目の設定内容を受け取る
//		String password = request.getParameter("password");
//		String passwordCheck = request.getParameter("passwordCheck");
//		String name = request.getParameter("name");
//		String birthDate = request.getParameter("birthDate");
//
//		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
//		dispatcher.forward(request, response);
//
//		//済 ログインセッションがない場合ログイン画面にリダイレクト
//
//		HttpSession session = request.getSession(false);
//		if(session == null) {
//			response.sendRedirect("/WEB-INF/jsp/login.jsp");
//		}
//
//		//確認用パスワードが違った場合
//		if(!password.equals(passwordCheck) ) {
//			//リクエストスコープにエラーメッセージをセット
//			request.setAttribute("errMsg", "入力した内容は正しくありません");
//		}
//
//		//パスワードが空欄だった場合
//		else if (password.equals("")) {
//
//			//リクエストパラメータの入力項目を引数に渡して、Daoのメゾッドを実行
//			UserDao userDao = new UserDao();
//			List<User> user = userDao.update(name, birthDate);
//
//			//更新失敗の場合
//			if(user == null) {
//				//リクエストスコープにエラーメッセージをセット
//				request.setAttribute("errMsg", "ログインに失敗しました。");
//
//				//update.jspにフォワード
//				dispatcher.forward(request, response);
//				return;
//			}
//
//			//更新成功の場合
//			//セッションにユーザ情報をセット
//			session.setAttribute("userInfo", user);
//
//			//ユーザ一覧サーブレットに
//			RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
//			dispatcher.forward(request, response);
//			return;
//		}
//
//		//パスワードの変更有の場合
//		else {
//			//リクエストパラメータの文字コード指定
//			request.setCharacterEncoding("UTF-8");
//
//			//リクエストパラメータの入力項目を引数に渡して、Daoのメゾッドを実行
//			UserDao userDao = new UserDao();
//			List<User> user = userDao.updateWithPassword(name, birthDate, password);
//
//			//セッションにユーザ情報をセット
//			session.setAttribute("userInfo", user);
//
//			//ユーザ一覧のサーブレットにリダイレクト
//			response.sendRedirect("UserListServlet");
//		}
	}

//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		request.setCharacterEncoding("utf-8");
//
//		//各項目の設定内容を受け取る
//		String password = request.getParameter("password");
//		String passwordCheck = request.getParameter("passwordCheck");
//		String name = request.getParameter("name");
//		String birthDate = request.getParameter("birthDate");
//
//		//確認用パスワードが違った場合
//		if(!password.equals(passwordCheck) ) {
//			//リクエストスコープにエラーメッセージをセット
//			request.setAttribute("errMsg", "入力した内容は正しくありません");
//		}
//
//		//パスワードが空欄だった場合
//		else if (password.equals("")) {
//
//			//リクエストパラメータの入力項目を引数に渡して、Daoのメゾッドを実行
//			UserDao userDao = new UserDao();
//			List<User> user = userDao.update(name, birthDate);
//
//			//更新失敗の場合
//			if(user == null) {
//				//リクエストスコープにエラーメッセージをセット
//				request.setAttribute("errMsg", "ログインに失敗しました。");
//
//				//update.jspにフォワード
//				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
//				dispatcher.forward(request, response);
//				return;
//			}
//
//			//更新成功の場合
//			//セッションにユーザ情報をセット
//			HttpSession session = request.getSession();
//			session.setAttribute("userInfo", user);
//
//			//ユーザ一覧サーブレットに
//			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
//			dispatcher.forward(request, response);
//			return;
//		}
//
//		//パスワードの変更有の場合
//		else {
//			//リクエストパラメータの文字コード指定
//			request.setCharacterEncoding("UTF-8");
//
//			//リクエストパラメータの入力項目を引数に渡して、Daoのメゾッドを実行
//			UserDao userDao = new UserDao();
//			List<User> user = userDao.updateWithPassword(name, birthDate, password);
//
//			//テーブルにデータがない場合
//			if(user == null) {
//				//リクエストスコープにエラーメッセージをセット
//				request.setAttribute("errMsg", "ログインに失敗しました。");
//
//				//update.jspにフォワード
//				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
//				dispatcher.forward(request, response);
//				return;
//			}
//
//			//テーブルにデータがあった場合
//
//			//セッションにユーザ情報をセット
//			HttpSession session = request.getSession();
//			session.setAttribute("userInfo", user);
//
//			//ユーザ一覧のサーブレットにリダイレクト
//			response.sendRedirect("UserListServlet");
//		}
//
//	}

}
