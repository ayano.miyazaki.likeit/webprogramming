package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public UserListServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException{

		//済 ログインセッションがない場合ログイン画面にリダイレクト

		HttpSession session = request.getSession(false);
		if(session == null) {
			response.sendRedirect("/WEB-INF/jsp/login.jsp");
		}

		//ユーザ一覧取得
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		//リクエストスコープにユーザ一覧をセット
		request.setAttribute("userList", userList);

		//ユーザ一覧にjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException{
		//済? 検索処理全般を実装
		
		

	}

}