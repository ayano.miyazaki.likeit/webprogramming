package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/SignUpServlet")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SignUpServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログインセッションが無い場合ログイン画面にリダイレクト
		HttpSession session = request.getSession();
		if(session == null) {
			response.sendRedirect("/WEB-INF/jsp/login.jsp");
		}

		//リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		//list.jspからIDを受け取る
		String id = request.getParameter("id");

		//確認用:コンソールにIDを出力
		System.out.println(id);

		//リクエストパラメータの項目を引数に渡して、Daoのメゾッドを実行
		UserDao userDao = new UserDao();
		User user = userDao.findById(id);

		//ユーザ情報をセット
		session.setAttribute("user", user);

		//sign-up.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign-up.jsp");
		dispatcher.forward(request, response);
		return;
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//dopost

		//sign-up.jspの入力項目を取得
		String loginId = (String) request.getParameter("loginId");
		String password = (String) request.getParameter("password");
		String passwordCheck = (String) request.getParameter("passwordCheck");
		String name = (String) request.getParameter("name");
		String birthDate = (String) request.getParameter("birthDate");

		//登録・更新日付
		Calendar cl = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
		String updateDate = sdf.format(cl.getTime());
		String createDate = sdf.format(cl.getTime());

		//元データを検索
		UserDao userDao = new UserDao();
		String sameLoginId = userDao.findByLoginId(loginId);


		//登録に失敗orパスワードが確認と違うor未入力項目ありの場合
//		if(!(sameLoginId == null) || !password.equals(passwordCheck) || loginId.equals("") || password.equals("") || name.equals("") || birthDate.equals("")) {
		if(sameLoginId != null) {
			HttpSession session = request.getSession();

			//セッションスコープにエラーメッセージをセット
			session.setAttribute("errMsg", "入力された内容は正しくありません");

			session.setAttribute("loginId", loginId);
			session.setAttribute("name", name);
			session.setAttribute("birthDate", birthDate);

			//sign-up.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign-up.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(!password.equals(passwordCheck)) {
			HttpSession session = request.getSession();

			//セッションスコープにエラーメッセージをセット
			session.setAttribute("errMsg", "入力された内容は正しくありません");

			session.setAttribute("loginId", loginId);
			session.setAttribute("name", name);
			session.setAttribute("birthDate", birthDate);

			//sign-up.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign-up.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(loginId.equals("") || password.equals("") || name.equals("") || birthDate.equals("")){
			HttpSession session = request.getSession();

			//セッションスコープにエラーメッセージをセット
			session.setAttribute("errMsg", "入力された内容は正しくありません");

			session.setAttribute("loginId", loginId);
			session.setAttribute("name", name);
			session.setAttribute("birthDate", birthDate);

			//sign-up.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign-up.jsp");
			dispatcher.forward(request, response);
			return;
		}


		//jspの入力項目を引数にし、Daoのメゾッド実行
		List<User> user = userDao.signUp(loginId, name, birthDate, password, createDate, updateDate);



		//ユーザ一覧のサーブレットへ遷移
		response.sendRedirect("UserListServlet");

	}

}
