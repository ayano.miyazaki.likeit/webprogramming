package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;



public class UserDao {

	//IDに紐づくユーザ情報を返す
	public  User findById(String id) {

		Connection conn = null;
		try {
			//DBへ接続
			conn = DBManager.getConnection();

			//SELECT文準備
			String sql = "SELECT * FROM user WHERE id = ?";

			//実行して結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時の処理(主キーは1つなので1回のみ)
			if(!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String NameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");
			String createDateData = rs.getString("create_date");
			String updateDateData = rs.getString("update_date");
			return new User(loginIdData, NameData, birthDateData, createDateData, updateDateData);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;

		}finally {
			//DB切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ログインIDとパスワードに紐づくユーザ情報を返す
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			//DBへ接続
			conn = DBManager.getConnection();

			//SELECT文準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			//実行して結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,loginId);
			pStmt.setString(2,password);
			ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時の処理(主キーは1つなので1回のみ)
			if(!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;

		}finally {
			//DB切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//全てのユーザ情報を取得
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // 済TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE id != 1";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    //一致するログインIDを取得
    public	String findByLoginId(String loginId) {
    	Connection conn = null;
		String getLoginId = new String();

    	try {
    	conn = DBManager.getConnection();

    	String sql = "SELECT * FROM user WHERE login_id = ?";

    	//実行して結果を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1,loginId);
		ResultSet rs = pStmt.executeQuery();

		//ない時の処理
		if(!rs.next()) {
			return null;
		}

        getLoginId = rs.getString("login_id");

        //確認用:コンソールへ出力
        System.out.println(rs);



    	}catch(SQLException e){
			e.printStackTrace();
		} finally {
	        // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
		}

		return getLoginId;
    }


    //ユーザ新規登録
    	public List<User> signUp(String loginId, String name, String birthDate, String password, String createDate, String updateDate){
    		Connection conn = null;

    		try {
    			//データベース接続
    			conn = DBManager.getConnection();

    			//データ登録
    			String sql = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date) "
    					+ "VALUES(?, ?, ?, ?, ?, ?)";

    			//実行して結果表を取得
    			PreparedStatement pStmt = conn.prepareStatement(sql);
    			pStmt.setString(1, loginId);
    			pStmt.setString(2, name);
    			pStmt.setString(3, birthDate);
    			pStmt.setString(4, password);
    			pStmt.setString(5, createDate);
    			pStmt.setString(6, updateDate);
    			pStmt.executeUpdate();

    			String

    			//確認用:コンソールへ出力
    			System.out.println();

    		} catch(SQLException e){
    			e.printStackTrace();
    		} finally {
    	        // データベース切断
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
    		}

    		return null;
    	}

    //ユーザ情報更新(パスワード無し)
        public List<User> update(String name, String birthDate) {
        	 Connection conn = null;
             List<User> userList = new ArrayList<User>();

             try {
                 // データベースへ接続
                 conn = DBManager.getConnection();

                 // データ更新
                 String sql = "UPDATE user SET name = ?, birth_date = ? WHERE user.id = ?";

     			//実行して結果を取得
     			PreparedStatement pStmt = conn.prepareStatement(sql);
     			pStmt.setString(1,name);
     			pStmt.setString(2,birthDate);
     			ResultSet rs = pStmt.executeQuery();
     			User user = new User(name, birthDate);

             } catch (SQLException e) {
                 e.printStackTrace();
             } finally {
                 // データベース切断
                 if (conn != null) {
                     try {
                         conn.close();
                     } catch (SQLException e) {
                         e.printStackTrace();
                         return null;
                     }
                 }
             }
             return userList;
    }

    //ユーザ情報更新(パスワード有り)
        public List<User> updateWithPassword(String name, String birthDate, String password) {
            Connection conn = null;
            List<User> userList = new ArrayList<User>();

            try {
                // データベースへ接続
                conn = DBManager.getConnection();

                // データ更新
                String sql = "UPDATE user SET name = ?, birth_date = ?, password = ? WHERE user.id = ?";

      			//実行して結果表を取得
     			PreparedStatement pStmt = conn.prepareStatement(sql);
     			pStmt.setString(1,name);
     			pStmt.setString(2,birthDate);
     			pStmt.setString(3,password);
     			ResultSet rs = pStmt.executeQuery();

                //変更されたものをコンソールへ出力
                System.out.println(rs);

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                // データベース切断
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            }
            return userList;
        }
}
